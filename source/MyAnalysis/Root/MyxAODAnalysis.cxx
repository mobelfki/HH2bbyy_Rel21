#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>

#include <AsgTools/MessageCheck.h>

#include "PathResolver/PathResolver.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include <TMath.h>
#include <TH1.h>
#include <TH3.h>
#include <TF1.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TProfile.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODBTagging/BTagging.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "PathResolver/PathResolver.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/Vertex.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.

}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
//Histograms
/*
book (TProfile ("PtReco","Profile of PtReco correction versus Pt",100,0,500,0.8,1.2));
book (TH1F ("BJet_Number", "Number of B Jet", 10, -0.5, 9.5));
book (TH1F ("Photon_Number", "Number of Photon", 10, -0.5, 9.5));
book (TH1F ("DeltaR_jets", "#Delta_{jet,jet}",100,0,5));
book (TH1F ("Truth_Photon_Number", "Number of Truth Photons", 10, -0.5, 9.5));
book (TH1F ("Photon_Mass","Invariant mass of photons system",100,90,150));
book (TH1F ("Jet_Mass","Invariant mass of jets system",100,90,160));
book (TH1F ("Jet_Pt","Jet Pt",100,0,500));
book (TH1F ("Truth_Jet_Number", "Number of Truth Jets", 10, -0.5, 9.5));
book (TH1F ("Quark_Pt","Jet Pt",100,0,500));
*/

book (TH1F ("MU","Mu",100,-0.5,99.5));
book (TH2F ("MU_JVT","Numbre of jets pass JVT vs Mu",10,-0.5,9.5,20,-0.5,99.5));
book (TH2F ("MU_F","Numbre of forwod jets vs Mu",10,-0.5,9.5,20,-0.5,99.5));
book (TH2F ("MU_F_30","Numbre of forwod jets pass pt_30 cut vs Mu",10,-0.5,9.5,20,-0.5,99.5));
book (TProfile ("PtReco","Profile of PtReco correction versus Pt",50,0,500,0.8,1.2));
// Tree

ANA_CHECK (book (TTree ("Output", "")));
/*
tree ("Output")->Branch("ptb1",&ptb1);
tree ("Output")->Branch("ptb2",&ptb2);
tree ("Output")->Branch("ptj3",&ptj3);
tree ("Output")->Branch("pty1",&pty1);
tree ("Output")->Branch("pty2",&pty2);
tree ("Output")->Branch("ptyy",&ptyy);
tree ("Output")->Branch("phib1",&phib1);
tree ("Output")->Branch("phib2",&phib2);
tree ("Output")->Branch("phij3",&phij3);
tree ("Output")->Branch("phiy1",&phiy1);
tree ("Output")->Branch("phiy2",&phiy2);
tree ("Output")->Branch("etab1",&etab1);
tree ("Output")->Branch("etab2",&etab2);
tree ("Output")->Branch("etaj3",&etaj3);
tree ("Output")->Branch("etay1",&etay1);
tree ("Output")->Branch("etay2",&etay2);
tree ("Output")->Branch("my1",&my1);
tree ("Output")->Branch("my2",&my2);
tree ("Output")->Branch("eb1",&eb1);
tree ("Output")->Branch("eb2",&eb2);
tree ("Output")->Branch("ej3",&ej3);
tree ("Output")->Branch("njet",&njet);

*/

tree ("Output")->Branch("MC_BQuark_Pt",&MC_BQuark_Pt);
tree ("Output")->Branch("MC_BQuark_Eta",&MC_BQuark_Eta);
tree ("Output")->Branch("MC_BQuark_Phi",&MC_BQuark_Phi);
tree ("Output")->Branch("MC_BQuark_E",&MC_BQuark_E);
tree ("Output")->Branch("MC_BQuark_Size",&MC_BQuark_Size);

tree ("Output")->Branch("MC_Photon_Pt",&MC_Photon_Pt);
tree ("Output")->Branch("MC_Photon_Eta",&MC_Photon_Eta);
tree ("Output")->Branch("MC_Photon_Phi",&MC_Photon_Phi);
tree ("Output")->Branch("MC_Photon_E",&MC_Photon_E);
tree ("Output")->Branch("MC_Photon_Size",&MC_Photon_Size);

tree ("Output")->Branch("Truth_Photon_Pt",&Truth_Photon_Pt);
tree ("Output")->Branch("Truth_Photon_Eta",&Truth_Photon_Eta);
tree ("Output")->Branch("Truth_Photon_Phi",&Truth_Photon_Phi);
tree ("Output")->Branch("Truth_Photon_E",&Truth_Photon_E);
tree ("Output")->Branch("Truth_Photon_M",&Truth_Photon_M);
tree ("Output")->Branch("Truth_Photon_Size",&Truth_Photon_Size);

tree ("Output")->Branch("Truth_Jet_Pt",&Truth_Jet_Pt);
tree ("Output")->Branch("Truth_Jet_Eta",&Truth_Jet_Eta);
tree ("Output")->Branch("Truth_Jet_Phi",&Truth_Jet_Phi);
tree ("Output")->Branch("Truth_Jet_E",&Truth_Jet_E);
tree ("Output")->Branch("Truth_Jet_M",&Truth_Jet_M);
tree ("Output")->Branch("Truth_Jet_Size",&Truth_Jet_Size);

tree ("Output")->Branch("Truth_WZJet_Pt",&Truth_WZJet_Pt);
tree ("Output")->Branch("Truth_WZJet_Eta",&Truth_WZJet_Eta);
tree ("Output")->Branch("Truth_WZJet_Phi",&Truth_WZJet_Phi);
tree ("Output")->Branch("Truth_WZJet_E",&Truth_WZJet_E);
tree ("Output")->Branch("Truth_WZJet_ConeTruthLabelID",&Truth_WZJet_ConeTruthLabelID);
tree ("Output")->Branch("Truth_WZJet_Size",&Truth_WZJet_Size);

tree ("Output")->Branch("Reco_Photon_Pt",&Reco_Photon_Pt);
tree ("Output")->Branch("Reco_Photon_Eta",&Reco_Photon_Eta);
tree ("Output")->Branch("Reco_Photon_Phi",&Reco_Photon_Phi);
tree ("Output")->Branch("Reco_Photon_M",&Reco_Photon_M);
tree ("Output")->Branch("Reco_Photon_Ptiso",&Reco_Photon_Ptiso);
tree ("Output")->Branch("Reco_Photon_Eiso",&Reco_Photon_Eiso);
tree ("Output")->Branch("Reco_Photon_Size",&Reco_Photon_Size);
tree ("Output")->Branch("Reco_Photon_isTight",&Reco_Photon_isTight);
tree ("Output")->Branch("Reco_Photon_isIsolated",&Reco_Photon_isIsolated);


tree ("Output")->Branch("Reco_Jet_Pt",&Reco_Jet_Pt);
tree ("Output")->Branch("Reco_Jet_Eta",&Reco_Jet_Eta);
tree ("Output")->Branch("Reco_Jet_Phi",&Reco_Jet_Phi);
tree ("Output")->Branch("Reco_Jet_M",&Reco_Jet_M);
tree ("Output")->Branch("Reco_Jet_Size",&Reco_Jet_Size);
tree ("Output")->Branch("Reco_Jet_JVT",&Reco_Jet_JVT);
tree ("Output")->Branch("Reco_Jet_isBtagged",&Reco_Jet_isBtagged);

tree ("Output")->Branch("Reco_Muon_Pt",&Reco_Muon_Pt);
tree ("Output")->Branch("Reco_Muon_Eta",&Reco_Muon_Eta);
tree ("Output")->Branch("Reco_Muon_Phi",&Reco_Muon_Phi);
tree ("Output")->Branch("Reco_Muon_E",&Reco_Muon_E);
tree ("Output")->Branch("Reco_Muon_Size",&Reco_Muon_Size);
tree ("Output")->Branch("Reco_Muon_isTight",&Reco_Muon_isTight);

tree ("Output")->Branch("Reco_Electron_Pt",&Reco_Electron_Pt);
tree ("Output")->Branch("Reco_Electron_Eta",&Reco_Electron_Eta);
tree ("Output")->Branch("Reco_Electron_Phi",&Reco_Electron_Phi);
tree ("Output")->Branch("Reco_Electron_E",&Reco_Electron_E);
tree ("Output")->Branch("Reco_Electron_Size",&Reco_Electron_Size);
tree ("Output")->Branch("Reco_Electron_isLoose",&Reco_Electron_isLoose);
tree ("Output")->Branch("PU",&PU);

//Jet Calibration
  const std::string name = "MyxAODAnalysis"; 
  	 TString jetAlgo = "AntiKt4EMTopo" ; 
  	 TString config = "JES_data2016_data2015_Recommendation_Dec2016_rel21.config"; 
  	 TString calibSeq = "JetArea_Residual_EtaJES_GSC"; 
 	 bool isData = false; 

  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/JetCalibTools");
 	 if( !JetCalibrationTool_handle.isUserConfigured() ){
 	   ANA_CHECK( ASG_MAKE_ANA_TOOL(JetCalibrationTool_handle, JetCalibrationTool) );
 	   ANA_CHECK( JetCalibrationTool_handle.setProperty("JetCollection",jetAlgo.Data()) );
 	   ANA_CHECK( JetCalibrationTool_handle.setProperty("ConfigFile",config.Data()) );
    	   ANA_CHECK( JetCalibrationTool_handle.setProperty("CalibSequence",calibSeq.Data()) );
 	   ANA_CHECK( JetCalibrationTool_handle.setProperty("IsData",isData) );
    	   ANA_CHECK( JetCalibrationTool_handle.retrieve() );
  }

//Photon Identification
  m_photonTight = new AsgPhotonIsEMSelector ("PhotonTightIsEMSelector");
	ANA_CHECK( m_photonTight->setProperty("isEMMask", egammaPID::PhotonTight));
	ANA_CHECK( m_photonTight->setProperty("ConfigFile","/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf"));
	if(!m_photonTight->initialize().isSuccess()){
		Fatal("MyFunction", "Failed to initialize PhotonTightIsEMSelector");
	}

//Photon Isolation 
   iso_tool = new CP::IsolationSelectionTool("iso_tool");
  	ANA_CHECK( iso_tool->setProperty("PhotonWP","FixedCutLoose"));
	ANA_CHECK( iso_tool->initialize());

//Btagging Tool

   btagtool = new BTaggingSelectionTool("BTaggingSelectionTool");
 	 ANA_CHECK( btagtool->setProperty("MaxEta", 2.5));
  	 ANA_CHECK( btagtool->setProperty("MinPt", 20000.));
 	 ANA_CHECK( btagtool->setProperty("JetAuthor","AntiKt4EMTopoJets"));
 	 ANA_CHECK( btagtool->setProperty("TaggerName", "MV2c10"));
 	 ANA_CHECK( btagtool->setProperty("FlvTagCutDefinitionsFileName", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-02-09_v1.root"));
 	 ANA_CHECK( btagtool->setProperty("OperatingPoint", "FixedCutBEff_70"));
  	 ANA_CHECK( btagtool->initialize());

// Muon Selector 

	ANA_CHECK( m_muonSelection.setProperty( "MaxEta", 2.5 ));
        ANA_CHECK( m_muonSelection.setProperty( "MuQuality", 0));
	ANA_CHECK( m_muonSelection.setProperty("TurnOffMomCorr",true));
        ANA_CHECK( m_muonSelection.initialize());

//Electron identification 

	m_LooseLH = new AsgElectronLikelihoodTool ("LooseLH");
	ANA_CHECK( m_LooseLH->setProperty("WorkingPoint", "LooseLHElectron"));
	ANA_CHECK( m_LooseLH->setProperty("ConfigFile", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf"));
	ANA_CHECK( m_LooseLH->initialize());
// PileUp 

	pileup_tool.setTypeAndName("CP::PileupReweightingTool/tool");
	std::vector<std::string> LumicalcFiles;
	LumicalcFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180309/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root");
	std::vector<std::string> pileup_file;
	pileup_file.push_back("/eos/user/m/mobelfki/PileUp_NTUP_lmabda_plus10/mc16_13TeV.344731.MadGraphPythia8EvtGen_A14NNPDF23LO_hh_yybb_plus_lambda10.merge.NTUP_PILEUP.e5504_e5984_a875_r10201_r10210_p3384_p3385/NTUP_PILEUP.14017003._000001.pool.root.1");
	ANA_CHECK(pileup_tool.setProperty("ConfigFiles",pileup_file));
	ANA_CHECK(pileup_tool.setProperty("LumiCalcFiles",LumicalcFiles));
	ANA_CHECK(pileup_tool.initialize());

	nevent =0;
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
xAOD::TStore store;
  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  const DataVector<xAOD::Jet> *jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMTopoJets"));
  const DataVector<xAOD::Photon> *photons = nullptr;
  ANA_CHECK(evtStore()->retrieve( photons, "Photons" ));
  const DataVector<xAOD::Muon> *muons = nullptr;
  ANA_CHECK(evtStore()->retrieve( muons, "Muons"));
  const DataVector<xAOD::Electron> *electrons = nullptr;
  ANA_CHECK(evtStore()->retrieve( electrons, "Electrons")); 
  const DataVector<xAOD::Jet> *truthwz = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthwz, "AntiKt4TruthWZJets")); 
  xAOD::Muon::Quality muon_quality;
  const DataVector<xAOD::Jet> *truthjets = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthjets, "AntiKt4TruthJets"));
  const DataVector<xAOD::TruthParticle> *truthphotons = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthphotons, "egammaTruthParticles")); 
  const xAOD::TruthEventContainer *truthevent = nullptr;
  ANA_CHECK( evtStore()->retrieve( truthevent, "TruthEvents"));
  ANA_CHECK(pileup_tool->apply(*eventInfo)); //pileup

	//Variable decelaration
	TLorentzVector photon,jet,alljet,electron,muon,bjet1plusmuon,bjet2plusmuon,bjet1pluselectron,bjet2pluselectron,jet3,b,b_object,tlz_jet,tlz_f_jet,tlz_f_jvt_jet,p,l;
	std::vector<TLorentzVector> photonsvector, jetsvector, alljetsvector,electronsvector,muonsvector,truthphotonsvector,truthjetsvector,bquark,bjet,jvt_jet,f_jet,f_jvt_jet,mcphoton,lepton;
	std::vector<Float_t> jetsdeltar;
	//NTuple Variable
	ptb1 = 0., ptb2 = 0., ptj3 = 0., pty1 = 0., pty2 = 0., ptyy = 0., phib1 = 0., phib2 = 0., phij3 = 0., phiy1 = 0., phiy2 = 0., etab1 = 0., etab2 = 0., etaj3 = 0., etay1 = 0., etay2 = 0., my1 = 0., my2 = 0., eb1 = 0., eb2 = 0., ej3 = 0., njet = 0.; 
	

   MC_BQuark_Pt.clear();
   MC_BQuark_Eta.clear();
   MC_BQuark_Phi.clear();
   MC_BQuark_E.clear();
   MC_BQuark_Size = 0.0;
	
   MC_Photon_Pt.clear();
   MC_Photon_Eta.clear();
   MC_Photon_Phi.clear();
   MC_Photon_E.clear();
   MC_Photon_Size = 0.0;

   Truth_Photon_Pt.clear();
   Truth_Photon_Eta.clear();
   Truth_Photon_Phi.clear();
   Truth_Photon_E.clear();
   Truth_Photon_M.clear();
   Truth_Photon_Size = 0.0;

   Truth_Jet_Pt.clear();
   Truth_Jet_Eta.clear();
   Truth_Jet_Phi.clear();
   Truth_Jet_E.clear();
   Truth_Jet_M.clear();
   Truth_Jet_Size = 0.0;
  
   Reco_Photon_Pt.clear();
   Reco_Photon_Eta.clear();
   Reco_Photon_Phi.clear();
   Reco_Photon_M.clear();
   Reco_Photon_Ptiso.clear();
   Reco_Photon_Eiso.clear();
   Reco_Photon_isTight.clear();
   Reco_Photon_isIsolated.clear();
   Reco_Photon_Size = 0.0;

   Reco_Jet_Pt.clear();
   Reco_Jet_Eta.clear();
   Reco_Jet_Phi.clear();
   Reco_Jet_M.clear();
   Reco_Jet_JVT.clear();
   Reco_Jet_isBtagged.clear();
   Reco_Jet_Size = 0.0;

   Reco_Muon_Pt.clear();
   Reco_Muon_Eta.clear();
   Reco_Muon_Phi.clear();
   Reco_Muon_E.clear();
   Reco_Muon_Size = 0.0;
   Reco_Muon_isTight.clear();

   Reco_Electron_Pt.clear();
   Reco_Electron_Eta.clear();
   Reco_Electron_Phi.clear();
   Reco_Electron_E.clear();
   Reco_Electron_Size = 0.0;  
   Reco_Electron_isLoose.clear();

  Truth_WZJet_Pt.clear();
  Truth_WZJet_Eta.clear();
  Truth_WZJet_Phi.clear();
  Truth_WZJet_E.clear();
  Truth_WZJet_ConeTruthLabelID.clear();
  Truth_WZJet_Size = 0.0;
  PU = 0.0;


	Double_t MU = pileup_tool->getCorrectedActualInteractionsPerCrossing(*eventInfo, true);
	PU = MU;

//Computing of PTReco factor 
	xAOD::TruthEventContainer::const_iterator itr;
	for(itr = truthevent->begin(); itr!=truthevent->end(); ++itr){
		const xAOD::TruthParticle* truthparticle = NULL;
		const xAOD::TruthVertex* truthvertex = NULL;
		int nvertex = (*itr)->nTruthVertices();
		int nparticle = (*itr)->nTruthParticles();
		for(int i = 0; i<nparticle; i++){
				truthparticle = (*itr)->truthParticle(i);
			if(truthparticle != NULL){
				bool decay = truthparticle->hasDecayVtx();
				int id = truthparticle->pdgId();
				if(decay && id == 25){
					int nb = 0;
					int ny = 0;
					truthvertex = truthparticle->decayVtx();
					int nop = truthvertex->nOutgoingParticles(); 
					for(int j = 0; j<nop; j++){
					  	const xAOD::TruthParticle* prodparticle = truthvertex->outgoingParticle(j);
						int prodid = fabs(prodparticle->pdgId());
						if(prodid == 5 && (nb <2)){
							b.SetPxPyPzE(prodparticle->px()*0.001,prodparticle->py()*0.001,prodparticle->pz()*0.001,prodparticle->e()*0.001);
							bquark.push_back(b);
							nb++;
						}
						if(prodid == 22 && (ny <2)){
							p.SetPxPyPzE(prodparticle->px()*0.001,prodparticle->py()*0.001,prodparticle->pz()*0.001,prodparticle->e()*0.001);
							mcphoton.push_back(p);
							ny++;
						}
					}
				}	
			}		
		}
	}
	MC_BQuark_Size = bquark.size();
	for(unsigned int i = 0; i < bquark.size(); i++){
		MC_BQuark_Pt.push_back(bquark[i].Pt());
		MC_BQuark_Eta.push_back(bquark[i].Eta());
		MC_BQuark_Phi.push_back(bquark[i].Phi());
		MC_BQuark_E.push_back(bquark[i].E());
	}
	MC_Photon_Size = mcphoton.size();
	for(unsigned int i = 0; i < mcphoton.size(); i++){
		MC_Photon_Pt.push_back(mcphoton[i].Pt());
		MC_Photon_Eta.push_back(mcphoton[i].Eta());
		MC_Photon_Phi.push_back(mcphoton[i].Phi());
		MC_Photon_E.push_back(mcphoton[i].E());
	}
	
	for(unsigned int i = 0; i < jets->size(); i++){
		xAOD::Jet * calibratedjet2 = 0;
		float jvt = jets->get(i)->auxdata<float>("Jvt");
		float fabseta = fabs(jets->get(i)->eta());
		float pt = jets->get(i)->pt()*0.001; 
		if((fabseta < 2.4 && pt > 25 && fabs(jvt) > 0.59)){
		JetCalibrationTool_handle->calibratedCopy(*(jets->get(i)),calibratedjet2); 
		bool isBTagged = btagtool->accept(calibratedjet2);
		if(isBTagged){
		b_object.SetPtEtaPhiM((calibratedjet2->pt())*0.001,(calibratedjet2->eta()),(calibratedjet2->phi()),(calibratedjet2->m())*0.001);
		bjet.push_back(b_object);
        	}
		}
		delete calibratedjet2;
	}
	if(bjet.size() == 2 && bquark.size() == 2){
	if( bquark[0].DeltaR(bjet[0]) < 0.4 && bquark[1].DeltaR(bjet[1]) < 0.4 ){
		
		hist("PtReco")->Fill(bjet[0].Pt(),bquark[0].Pt()/bjet[0].Pt());
		hist("PtReco")->Fill(bjet[1].Pt(),bquark[1].Pt()/bjet[1].Pt());	
		
	}else if(bquark[0].DeltaR(bjet[1]) < 0.4 && bquark[1].DeltaR(bjet[0]) < 0.4){
		hist("PtReco")->Fill(bjet[0].Pt(),bquark[1].Pt()/bjet[0].Pt());
		hist("PtReco")->Fill(bjet[1].Pt(),bquark[0].Pt()/bjet[1].Pt());	
	}}


//Truth Analysis

	truthphotonsvector = PhotonsReader(truthphotons);	
	truthjetsvector = JetsReader(truthjets);
	jetsdeltar = Comp_DeltaR(truthjetsvector);

	//hist("Truth_Photon_Number")->Fill(truthphotonsvector.size());
	//hist("Truth_Jet_Number")->Fill(truthjetsvector.size());	

// Fill tree

	for(unsigned int i = 0; i<truthphotonsvector.size(); i++)
	{
		Truth_Photon_Pt.push_back(truthphotonsvector[i].Pt());
		Truth_Photon_Eta.push_back(truthphotonsvector[i].Eta());
		Truth_Photon_Phi.push_back(truthphotonsvector[i].Phi());
		Truth_Photon_E.push_back(truthphotonsvector[i].E());
		Truth_Photon_M.push_back(truthphotonsvector[i].M());
	}
		Truth_Photon_Size = truthphotonsvector.size();

       	for(unsigned int i = 0; i<truthjetsvector.size(); i++)
	{
		Truth_Jet_Pt.push_back(truthjetsvector[i].Pt());
		Truth_Jet_Eta.push_back(truthjetsvector[i].Eta());
		Truth_Jet_Phi.push_back(truthjetsvector[i].Phi());
		Truth_Jet_E.push_back(truthjetsvector[i].E());
		Truth_Jet_M.push_back(truthjetsvector[i].M());
	}
		Truth_Jet_Size = truthjetsvector.size();


	if(truthphotonsvector.size() == 2)
	{
		//hist("Photon_Mass")->Fill((truthphotonsvector[0]+truthphotonsvector[1]).M());
	}
	if(truthjetsvector.size() == 2)
	{
		//hist("Jet_Mass")->Fill((truthjetsvector[0]+truthjetsvector[1]).M());
	}
	for( unsigned int i = 0; i < truthjetsvector.size(); i++)
	{
		//hist("Jet_Pt")->Fill((truthjetsvector[i]).Pt());	
	}
	for( unsigned int i = 0; i < jetsdeltar.size(); i++)
	{
		//hist("DeltaR_jets")->Fill(jetsdeltar[i]);	
	}

// Create Variables 

	



//Reconstructed Analysis

	for( unsigned int i = 0; i < electrons->size(); i++){
	 	bool isLHLoose = m_LooseLH->accept(electrons->get(i));
		
		Reco_Electron_Pt.push_back(((electrons->get(i))->pt())*0.001);
		Reco_Electron_Eta.push_back(((electrons->get(i))->eta()));
		Reco_Electron_Phi.push_back(((electrons->get(i))->phi()));
		Reco_Electron_E.push_back(((electrons->get(i))->e())*0.001);
		Reco_Electron_isLoose.push_back(isLHLoose);
		
		//if(isLHLoose && ((electrons->get(i))->pt())*0.001 > 5){
		electron.SetPtEtaPhiE(((electrons->get(i))->pt())*0.001,((electrons->get(i))->eta()),((electrons->get(i))->phi()),((electrons->get(i))->e())*0.001);
		electronsvector.push_back(electron);	
	//}
	}	
		Reco_Electron_Size = electronsvector.size();

	for(unsigned int i = 0; i < muons->size(); i++){
		muon_quality = m_muonSelection.getQuality(*muons->get(i));
		bool isTight = (muon_quality == xAOD::Muon::Tight);

		Reco_Muon_Pt.push_back(((muons->get(i))->pt())*0.001);
		Reco_Muon_Eta.push_back(((muons->get(i))->eta()));
		Reco_Muon_Phi.push_back(((muons->get(i))->phi()));
		Reco_Muon_E.push_back(((muons->get(i))->e())*0.001);
		Reco_Muon_isTight.push_back(isTight);

	//	if(muon_quality == xAOD::Muon::Medium && ((muons->get(i))->pt())*0.001 > 5){
		muon.SetPtEtaPhiE(((muons->get(i))->pt())*0.001,((muons->get(i))->eta()),((muons->get(i))->phi()),((muons->get(i))->e())*0.001);
		muonsvector.push_back(muon);
	//}
	}
		Reco_Muon_Size = muonsvector.size();

	for(unsigned int i = 0; i < photons->size(); i++){
		float ptiso = 0.000001*(photons->get(i))->auxdata<float>("ptcone20");
		float etiso = 0.000001*(photons->get(i))->auxdata<float>("topoetcone20");
		float pt = (photons->get(i))->pt()*0.001;
		float et = (photons->get(i))->pt()*0.001;
	//	if((ptiso < 0.05*pt) && (etiso < 0.065*et)){
	//	if( fabs((photons->get(i))->eta()) < 1.37 || ( fabs((photons->get(i))->eta()) > 1.5 && fabs((photons->get(i))->eta()) < 2.37 ) ){
		bool isTight = m_photonTight->accept(photons->get(i));
		bool isIsolated = iso_tool->accept(*photons->get(i));

		Reco_Photon_Pt.push_back((photons->get(i))->pt()*0.001);
		Reco_Photon_Eta.push_back((photons->get(i))->eta());
		Reco_Photon_Phi.push_back((photons->get(i))->phi());
		Reco_Photon_M.push_back(0.0);
		Reco_Photon_isTight.push_back(isTight);
		Reco_Photon_isIsolated.push_back(isIsolated);
		Reco_Photon_Ptiso.push_back(ptiso);
		Reco_Photon_Eiso.push_back(etiso);

	//	if(isIsolated && isTight){
		photon.SetPtEtaPhiM(((photons->get(i))->pt())*0.001,((photons->get(i))->eta()),((photons->get(i))->phi()),0.0);
		photonsvector.push_back(photon);
	}//}//}//}
		Reco_Photon_Size = photonsvector.size();

	//hist("Photon_Number")->Fill(photonsvector.size());
	for(unsigned int i = 0; i < jets->size(); i++){
		xAOD::Jet * calibratedjet = 0;
		float jvt = jets->get(i)->auxdata<float>("Jvt");
		float fabseta = fabs(jets->get(i)->eta());
		float pt = jets->get(i)->pt()*0.001; 
//		if((fabseta < 2.4 && pt > 20 && fabs(jvt) > 0.59) || (fabseta > 2.4 && pt > 30)){
		JetCalibrationTool_handle->calibratedCopy(*(jets->get(i)),calibratedjet); 
		alljet.SetPtEtaPhiM((calibratedjet->pt())*0.001,(calibratedjet->eta()),(calibratedjet->phi()),(calibratedjet->m())*0.001);
		alljetsvector.push_back(alljet);
		bool isBTagged = btagtool->accept(calibratedjet);

		Reco_Jet_Pt.push_back((calibratedjet->pt())*0.001);
		Reco_Jet_Eta.push_back((calibratedjet->eta()));
		Reco_Jet_Phi.push_back((calibratedjet->phi()));
		Reco_Jet_JVT.push_back(jvt);
		Reco_Jet_M.push_back((calibratedjet->m())*0.001);		
		Reco_Jet_isBtagged.push_back(isBTagged);		

		if(fabs(jvt) > 0.59)
		{
			tlz_jet.SetPtEtaPhiM((calibratedjet->pt())*0.001,(calibratedjet->eta()),(calibratedjet->phi()),(calibratedjet->m())*0.001);
			jvt_jet.push_back(tlz_jet);
		}
		
		if(fabseta > 2.4)
		{
			tlz_f_jet.SetPtEtaPhiM((calibratedjet->pt())*0.001,(calibratedjet->eta()),(calibratedjet->phi()),(calibratedjet->m())*0.001);
			f_jet.push_back(tlz_f_jet);
		}
		if(fabseta > 2.4 && pt > 30)
		{
			tlz_f_jvt_jet.SetPtEtaPhiM((calibratedjet->pt())*0.001,(calibratedjet->eta()),(calibratedjet->phi()),(calibratedjet->m())*0.001);
			f_jvt_jet.push_back(tlz_f_jvt_jet);
		}
//		if(isBTagged){
		jet.SetPtEtaPhiM((calibratedjet->pt())*0.001,(calibratedjet->eta()),(calibratedjet->phi()),(calibratedjet->m())*0.001);
		jetsvector.push_back(jet);
//		}
//		}
		delete calibratedjet;
	}
	//hist("BJet_Number")->Fill(jetsvector.size());

		Reco_Jet_Size = jetsvector.size();

	hist("MU")->Fill(MU);
	hist("MU_JVT")->Fill(jvt_jet.size(),MU);
	hist("MU_F")->Fill(f_jet.size(),MU);
	hist("MU_F_30")->Fill(f_jvt_jet.size(),MU);


	if(jetsvector.size() == 2){
			bjet1pluselectron = jetsvector[0];
			bjet2pluselectron = jetsvector[1];
		for(unsigned int i = 0; i<electronsvector.size(); i++){
			if(jetsvector[0].DeltaR(electronsvector[i]) < 0.4){
				bjet1pluselectron = bjet1pluselectron + electronsvector[i];
			}else if(jetsvector[1].DeltaR(electronsvector[i]) < 0.4){
				bjet2pluselectron = bjet2pluselectron + electronsvector[i];
			}
			jetsvector[0] = bjet1pluselectron;
			jetsvector[1] = bjet2pluselectron;
		}
		}
		if(jetsvector.size() == 2){
			bjet1plusmuon = jetsvector[0];
			bjet2plusmuon = jetsvector[1];
		for(unsigned int i = 0; i<muonsvector.size(); i++){
			if(jetsvector[0].DeltaR(muonsvector[i]) < 0.4){
				bjet1plusmuon = bjet1plusmuon + muonsvector[i];
			}else if(jetsvector[1].DeltaR(muonsvector[i]) < 0.4){
				bjet2plusmuon = bjet2plusmuon + muonsvector[i];
			}
			jetsvector[0] = bjet1plusmuon;
			jetsvector[1] = bjet2plusmuon;
		}
		}
	
	for( unsigned int i = 0; i<truthwz->size(); i++)
	{
		Truth_WZJet_Pt.push_back((truthwz->get(i))->pt()*0.001);
		Truth_WZJet_Eta.push_back((truthwz->get(i))->eta());
		Truth_WZJet_Phi.push_back((truthwz->get(i))->phi());
		Truth_WZJet_E.push_back((truthwz->get(i))->e()*0.001);
		Truth_WZJet_ConeTruthLabelID.push_back((truthwz->get(i))->auxdata<int>("ConeTruthLabelID"));		
	}
		Truth_WZJet_Size = truthwz->size();


	for(unsigned int i = 0; i<jetsvector.size(); i++){
		for(unsigned int j = 0; j<truthwz->size(); j++){
			float ptreco = ((truthwz->get(j))->pt()*0.001)/jetsvector[i].Pt();
		//	hist("PtReco")->Fill(jetsvector[i].Pt(),ptreco);
		}
	}
	if(photonsvector.size() == 2 && jetsvector.size() == 2){
	//B-Quarks
		ptb1 = jetsvector[0].Pt();
		ptb2 = jetsvector[1].Pt();
		phib1 = jetsvector[0].Phi();
		phib2 = jetsvector[1].Phi();
		etab1 = jetsvector[0].Eta();
		etab2 = jetsvector[1].Eta();
		eb1 = jetsvector[0].E();
		eb2 = jetsvector[1].E();
	//Photons
		pty1 = photonsvector[0].Pt();
		pty2 = photonsvector[1].Pt();
		phiy1 = photonsvector[0].Phi();
		phiy2 = photonsvector[1].Phi();
		etay1 = photonsvector[0].Eta();
		etay2 = photonsvector[1].Eta();
		my1 = 0.;
		my2 = 0.;
		ptyy = (photonsvector[0]+photonsvector[1]).Pt();
	//Additional Jet
		njet = alljetsvector.size();
		if(alljetsvector.size() == 3){
			for(unsigned int i = 0; i<alljetsvector.size(); i++){
			  if(alljetsvector[i].Pt() != jetsvector[0].Pt() && alljetsvector[i].Pt() != jetsvector[1].Pt() && alljetsvector[i].Eta() != jetsvector[0].Eta() && alljetsvector[i].Eta() != jetsvector[1].Eta()){
				ptj3 = alljetsvector[i].Pt();
				phij3 = alljetsvector[i].Phi();
				etaj3 = alljetsvector[i].Eta();
				ej3 = alljetsvector[i].E();
			}}
		}
	//   tree("Output")->Fill();
	}
  tree("Output")->Fill();
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
//std::cout<<"Number of selected events is : "<< nevent << std::endl; 
  return StatusCode::SUCCESS;
}
std::vector<Float_t> MyxAODAnalysis :: Comp_DeltaR(std::vector<TLorentzVector> jets)
{
	std::vector<Float_t> result;
	for(unsigned int i = 0; i < jets.size(); i++)
	{
		for(unsigned int j = i+1; j < jets.size(); j++)
		{
			Float_t deltaR = jets[i].DeltaR(jets[j]);
			result.push_back(deltaR);			
		}
	}
	return result;
}

std::vector<TLorentzVector> MyxAODAnalysis :: JetsReader(const DataVector<xAOD::Jet>* jets)
{
	TLorentzVector tlz;
	std::vector<TLorentzVector> tlzvector;
	for (unsigned int i = 0; i < jets->size(); i++)
	{
		//Select b-jets
		if( (jets->get(i))->auxdata<int>("HadronConeExclTruthLabelID") == 5 && (jets->get(i)->pt())*0.001 > 30.)
		{
			tlz.SetPtEtaPhiM((jets->get(i)->pt())*0.001,(jets->get(i)->eta()),(jets->get(i)->phi()),(jets->get(i)->m())*0.001);
			tlzvector.push_back(tlz);
		}
	}
		return tlzvector;
}

std::vector<TLorentzVector> MyxAODAnalysis :: PhotonsReader(const DataVector<xAOD::TruthParticle>* photons)
{
	TLorentzVector tlz;
	std::vector<TLorentzVector> tlzvector;
	for (unsigned int i = 0; i < photons->size(); i++)
	{
		//Photon from Higgs
		//if( fabs((photons->get(i))->auxdata<int>("pdgId")) == 11)
		if( (photons->get(i))->auxdata<int>("truthOrigin") == 14)
		{
			tlz.SetPtEtaPhiM((photons->get(i)->pt())*0.001,(photons->get(i)->eta()),(photons->get(i)->phi()),(photons->get(i)->m())*0.001);
			tlzvector.push_back(tlz);
		}
	}
		return tlzvector;
}
