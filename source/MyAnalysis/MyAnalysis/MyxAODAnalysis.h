#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include <TTree.h>
#include <vector>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
	
  std::vector<Float_t> Comp_DeltaR(std::vector<TLorentzVector> jets);
  std::vector<TLorentzVector> JetsReader(const DataVector<xAOD::Jet>* jets);
  std::vector<TLorentzVector> PhotonsReader(const DataVector<xAOD::TruthParticle>* photons);

  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
  AsgPhotonIsEMSelector* m_photonTight;
  CP::IsolationSelectionTool*  iso_tool;
  BTaggingSelectionTool* btagtool;
  CP::MuonSelectionTool m_muonSelection;
  AsgElectronLikelihoodTool* m_LooseLH;
	
  asg::AnaToolHandle<CP::IPileupReweightingTool> pileup_tool;

 
  int nevent;
  //NTuple Variable

  float ptb1;
  float ptb2;
  float ptj3;
  float pty1;
  float pty2;
  float ptyy;
  float phib1;
  float phib2;
  float phij3;
  float phiy1;
  float phiy2;
  float etab1;
  float etab2;
  float etaj3;
  float etay1;
  float etay2;
  float my1;
  float my2;
  float eb1;
  float eb2;
  float ej3;
  float njet;


 
  std::vector<Float_t> MC_BQuark_Pt;
  std::vector<Float_t> MC_BQuark_Eta;
  std::vector<Float_t> MC_BQuark_Phi;
  std::vector<Float_t> MC_BQuark_E;
  Float_t MC_BQuark_Size;

  std::vector<Float_t> MC_Photon_Pt;
  std::vector<Float_t> MC_Photon_Eta;
  std::vector<Float_t> MC_Photon_Phi;
  std::vector<Float_t> MC_Photon_E;
  Float_t MC_Photon_Size;

  std::vector<Float_t> Truth_Photon_Pt;
  std::vector<Float_t> Truth_Photon_Eta;
  std::vector<Float_t> Truth_Photon_Phi;
  std::vector<Float_t> Truth_Photon_E;
  std::vector<Float_t> Truth_Photon_M;
  Float_t Truth_Photon_Size;

  std::vector<Float_t> Truth_Jet_Pt;
  std::vector<Float_t> Truth_Jet_Eta;
  std::vector<Float_t> Truth_Jet_Phi;
  std::vector<Float_t> Truth_Jet_E;
  std::vector<Float_t> Truth_Jet_M;
  Float_t Truth_Jet_Size;

  std::vector<Float_t> Truth_WZJet_Pt;
  std::vector<Float_t> Truth_WZJet_Eta;
  std::vector<Float_t> Truth_WZJet_Phi;
  std::vector<Float_t> Truth_WZJet_E;
  std::vector<Float_t> Truth_WZJet_ConeTruthLabelID;
  Float_t Truth_WZJet_Size;

  std::vector<Float_t> Reco_Photon_Pt;
  std::vector<Float_t> Reco_Photon_Eta;
  std::vector<Float_t> Reco_Photon_Phi;
  std::vector<Float_t> Reco_Photon_M;
  std::vector<Float_t> Reco_Photon_Ptiso;
  std::vector<Float_t> Reco_Photon_Eiso;
  std::vector<Bool_t> Reco_Photon_isTight;
  std::vector<Bool_t> Reco_Photon_isIsolated;
  Float_t Reco_Photon_Size;

  std::vector<Float_t> Reco_Jet_Pt;
  std::vector<Float_t> Reco_Jet_Eta;
  std::vector<Float_t> Reco_Jet_Phi;
  std::vector<Float_t> Reco_Jet_M;
  std::vector<Float_t> Reco_Jet_JVT;
  std::vector<Bool_t> Reco_Jet_isBtagged;
  Float_t Reco_Jet_Size;

  std::vector<Float_t> Reco_Muon_Pt;
  std::vector<Float_t> Reco_Muon_Eta;
  std::vector<Float_t> Reco_Muon_Phi;
  std::vector<Float_t> Reco_Muon_E;
  Float_t Reco_Muon_Size;
  std::vector<Bool_t> Reco_Muon_isTight;

  std::vector<Float_t> Reco_Electron_Pt;
  std::vector<Float_t> Reco_Electron_Eta;
  std::vector<Float_t> Reco_Electron_Phi;
  std::vector<Float_t> Reco_Electron_E;
  Float_t Reco_Electron_Size;  
  std::vector<Bool_t> Reco_Electron_isLoose;
  Float_t PU;


private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
};

#endif