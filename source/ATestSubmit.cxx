
void ATestSubmit (const std::string& submitDir)
{
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  //std::string dataPath = "/eos/user/m/mobelfki/test/mc16_13TeV/";
  //SH::ScanDir().scan(sh,dataPath);
 
  SH::scanRucio (sh, "mc16_13TeV.345934.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttH125_gamgam.deriv.DAOD_HIGG1D1.e6590_e5984_s3126_r10201_r10210_p3524");

  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  //job.options()->setDouble (EL::Job::optMaxEvents, 10000); // for testing purposes, limit to run over the first 500 events only!

 //Create NTuple

 job.outputAdd (EL::OutputStream ("ANALYSIS"));


  // add our algorithm to the job
  EL::AnaAlgorithmConfig config;
  config.setType ("MyxAODAnalysis");

  // set the name of the algorithm (this is the name use with
  // messages)
  config.setName ("AnalysisAlg");

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (config);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  //EL::DirectDriver driver;
  EL::PrunDriver driver;
// we can use other drivers to run things on the Grid, with PROOF, etc.

   driver.options()->setString("nc_outputSampleName", "user.mobelfki.testttH.%in:name345934%.%in:namee6590_e5984_s3126_r10201_r10210_p3524%");

   //driver.options()->setDouble(EL::Job::optGridMergeOutput, 1);
   char name[20];
  time_t now = time(0);
  strftime(name, sizeof(name), "%Y%m%d_%H%M%S", localtime(&now));
  std::stringstream ss;
  ss << submitDir <<"_"<<name;
  
  std::cout << "submitDir "<<ss.str()<<std::endl;
  driver.submit (job, ss.str());

  // process the job using the driver
 // driver.submit (job, submitDir);
}

